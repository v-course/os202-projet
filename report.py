import subprocess
import re
import matplotlib.pyplot as plt
import os
import numpy as np
import pandas as pd
from tqdm import tqdm

os.system("rm -rf plots && mkdir plots")

PLOTS_PATH = "plots"

MAX_ITERATIONS = 50
MAX_LIFE = 500
SIZE = 64
ALPHA = 0.9
BETA = 0.99
n_processes = range(1, 9)
n_tasks = [8, 16]

parameters_to_analyse = ["Average FPS"]


def get_numeric_value(str):
    result = re.findall("\d+\.*\d+", str)

    if len(result) == 0:
        raise Exception(f"Cannot find match for numeric value in {str}")
    if len(result) > 1:
        raise Exception(f"More than 1 match for numeric value in {str}")

    return float(result[0])


def get_metrics_values(output, metric_names):
    metrics = []

    for metric in metric_names:
        pattern = f"{metric}[\s]*:[\s]*[\d]+[\.]?[\d]+"
        result = re.findall(pattern, output)

        if len(result) == 0:
            raise Exception(f"Cannot find match for {metric} in ", output)
        if len(result) > 1:
            raise Exception(f"More than 1 match for {metric}: ", result)

        metric_value = get_numeric_value(result[0])
        metrics.append(metric_value)

    return metrics


fps = []


# Format of commands:
# python3 ants.py [height] [width] [max_life] [alpha] [beta] [max_iterations=n]
# mpirun -n [number_of_processes] python3 ./mpi.py [height] [width] [max_life] [alpha] [beta] [max_iterations=n] [n_tasks=m]
def get_fps(parameters, n_tasks=None):
    config = [
        str(parameters["size"]),
        str(parameters["size"]),
        str(MAX_LIFE),
        str(parameters["alpha"]),
        str(parameters["beta"]),
        f"max_iterations={MAX_ITERATIONS}",
    ]

    fps = []

    for n in tqdm(n_processes):
        proc = None

        # No parallelisation
        if n == 1:
            proc = subprocess.Popen(
                ["python3", "ants.py"] + config,
                stdout=subprocess.PIPE,
                stderr=subprocess.STDOUT,
            )
        else:
            proc = subprocess.Popen(
                [
                    "mpirun",
                    "-n",
                    str(n),
                    "--hostfile",
                    "hostfile",
                    "python3",
                    "mpi.py",
                ]
                + config
                + [f"n_tasks={n_tasks if n_tasks is not None else max(n - 1, 2)}"],
                stdout=subprocess.PIPE,
                stderr=subprocess.STDOUT,
            )

        out = proc.communicate()[0].decode("utf-8")

        metrics = get_metrics_values(out, parameters_to_analyse)
        fps.append(metrics[0])

    return fps


def get_speedup(fps):
    speedup = []
    for i in range(len(fps)):
        speedup.append(fps[0] / fps[i] * 100)
    return speedup


def plot_fps_and_speedup(
    fps,
    speedup,
    title,
    x_axis=n_processes,
    x_label="Nombre de processeurs",
    y1_label="FPS",
    y2_label="Speed-up (%)",
):
    plt.ylim(bottom=0)
    fig, (ax1, ax2) = plt.subplots(1, 2, figsize=(10, 4.8))

    # Plot FPS
    x = np.arange(len(x_axis))
    slope, intercept = np.polyfit(n_processes, fps, 1)
    regression_line = slope * x + intercept

    ax1.bar(x_axis, fps, width=0.5)
    ax1.plot(x_axis, regression_line, color="orange")
    ax1.set_ylabel(y1_label)

    plt.xlabel(x_label)
    plt.xticks(x_axis)

    # Plot speed-up
    ax2.bar(x_axis, speedup, width=0.5, color="green")
    slope, intercept = np.polyfit(n_processes, speedup, 1)
    regression_line = slope * x + intercept
    ax2.plot(x_axis, regression_line, color="orange")
    ax2.set_ylabel(y2_label)

    plt.xticks(x_axis)

    # Save figure to file
    fig = plt.gcf()
    title = title.replace(" ", "_")
    fig_path = os.path.join(PLOTS_PATH, f"{title}.png")
    fig.savefig(fig_path)
    plt.clf()

    return fig_path


def generate_report_for_parameter(parameter_name, parameter_values, parameter_title):
    print(f"\nGenerating report for {parameter_name}...\n")
    with open("README.md", "a") as readme:
        for p in parameter_values:
            readme.write(f"\n\n### {parameter_title}: {p}\n")

            parameters = {"size": SIZE, "alpha": ALPHA, "beta": BETA}
            parameters[parameter_name] = p

            print(f"\n{parameter_name}: {p}...\n")

            # for n_t in tqdm(n_tasks):
            # readme.write(f"\n\n#### Le nombre de tâches: {n_t}\n")

            fps = get_fps(parameters)
            speedup = get_speedup(fps)
            title = (
                f"FPS en fonction de nombre de processeurs. {parameter_title}: {p}."
                # + f" Le nombre de tâches: {n_t}"
            )
            fig_path = plot_fps_and_speedup(
                fps=fps,
                speedup=speedup,
                title=title,
            )
            df = pd.DataFrame(
                {
                    "Nombre de processeurs": n_processes,
                    "FPS": fps,
                    "Speed-up (%)": speedup,
                }
            )
            readme.write(f"\n{df.to_markdown(index=False)}\n")
            readme.write(f"\n![{title}]({fig_path})\n")


sizes = [32, 64, 128]
alpha = [0.7, 0.9]
beta = [0.7, 0.99]

with open("README.md", "a") as readme:
    readme.write("\n## Résultats\n")
    readme.write(
        (
            "\nLes valuers par défaut:\n"
            + f"\n- La taille de labyrinthe: {SIZE}\n"
            + f"\n- Alpha: {ALPHA}\n"
            + f"\n- Beta: {BETA}\n"
        )
    )

generate_report_for_parameter("size", sizes, parameter_title="La taille de labyrinthe")
# generate_report_for_parameter("alpha", alpha, parameter_title="Alpha")
# generate_report_for_parameter("beta", beta, parameter_title="Beta")
