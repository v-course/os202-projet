from mpi4py import MPI
import sys
import time
import maze
import pheromone
from ants import Colony
from input_parameters import get_parameters

globCom = MPI.COMM_WORLD.Dup()
comm = MPI.COMM_WORLD
n_processes = globCom.size
rank = globCom.rank

WORK_TAG = 0
STOP_TAG = 1
DONE_TAG = 2
CHILD_EXIT_TAG = 3

# Get input parameters.
# Each process has its own copy of parameters (no significant storage costs)
parameters = get_parameters(sys.argv)


def master():
    n_colonies = n_processes
    if parameters["n_tasks"] is not None:
        n_colonies = parameters["n_tasks"]

    # Parallelisation:
    # Divide all ants into n_tasks colonies.
    # Each colony is placed in the same maze and has the same nest
    nb_ants = parameters["nb_ants"]
    n_ants_per_colony = nb_ants // n_colonies
    rest = nb_ants % n_colonies
    colonies = []
    for i in range(n_colonies):
        if i == n_colonies - 1:
            colonies.append(
                Colony(
                    n_ants_per_colony + rest,
                    parameters["pos_nest"],
                    parameters["max_life"],
                )
            )
        else:
            colonies.append(
                Colony(
                    n_ants_per_colony, parameters["pos_nest"], parameters["max_life"]
                )
            )

    a_maze = maze.Maze(parameters["size_laby"], 12345)

    pherom = pheromone.Pheromon(
        parameters["size_laby"],
        parameters["pos_food"],
        parameters["alpha"],
        parameters["beta"],
    )

    # import pygame as pg

    # pg.init()
    # screen = pg.display.set_mode(parameters["resolution"])
    # mazeImg = a_maze.display()

    food_counter = 0

    avg_FPS = None
    snapshop_taken = False
    it = 0
    max_iterations = parameters["max_iterations"]

    processed_colonies = []
    total_food_counter = 0

    while True:
        # for event in pg.event.get():
        #     if event.type == pg.QUIT:
        #         break

        it += 1

        if max_iterations is not None and it == max_iterations:
            break

        deb = time.time()

        # Send colonies to subprocesses to calculate next positions
        for i in range(1, n_processes):
            comm.send((colonies.pop(), a_maze.maze, pherom), dest=i, tag=WORK_TAG)

        while len(colonies) > 0:
            status = MPI.Status()
            colony, food_counter = comm.recv(
                source=MPI.ANY_SOURCE, tag=DONE_TAG, status=status
            )
            total_food_counter += food_counter
            processed_colonies.append(colony)
            comm.send(
                obj=(colonies.pop(), a_maze.maze, pherom),
                dest=status.Get_source(),
                tag=WORK_TAG,
            )

        for i in range(1, n_processes):
            colony, food_counter = comm.recv(source=MPI.ANY_SOURCE, tag=DONE_TAG)
            total_food_counter += food_counter
            processed_colonies.append(colony)

        # Display
        # pherom.display(screen)
        # screen.blit(mazeImg, (0, 0))
        # for colony in processed_colonies:
        #     colony.display(screen)
        # pg.display.update()
        pherom.do_evaporation(parameters["pos_food"])
        end = time.time()

        # Reset tasks queue (=colonies)
        colonies = processed_colonies
        processed_colonies = []

        # if food_counter == 1 and not snapshop_taken:
        #     pg.image.save(screen, "MyFirstFood.png")
        #     snapshop_taken = True
        # pg.time.wait(500)

        FPS = 1.0 / (end - deb)
        print(f"FPS : {FPS:6.2f}, nourriture : {food_counter:7d}", end="\r")
        if avg_FPS is None:
            avg_FPS = FPS
        else:
            avg_FPS = (avg_FPS + FPS) / 2

    print(f"\nAverage FPS : {avg_FPS:6.2f}, food : {food_counter:7d}")

    # Send stop to all subprocesses when loop is ended
    # (either max number of iterations reached or user has quit the game)
    for i in range(1, n_processes):
        comm.send(obj=None, dest=i, tag=STOP_TAG)

    # Wait for all subprocesses to end
    for i in range(1, n_processes):
        comm.recv(source=MPI.ANY_SOURCE, tag=CHILD_EXIT_TAG)

    # pg.quit()
    MPI.Finalize()
    exit(0)


def slave():
    status = MPI.Status()

    while True:
        # Receive colony and other parameters from the master process
        data = comm.recv(source=0, status=status)

        if status.Get_tag() == STOP_TAG:
            comm.send(
                obj=None, dest=0, tag=CHILD_EXIT_TAG
            )  # Send to the master process a signal that the child is exiting
            exit(0)  # Exit the loop and stop processing

        (colony, a_maze, pherom) = data

        # Calculate next colony positions
        food_counter = colony.advance(
            a_maze, parameters["pos_food"], parameters["pos_nest"], pherom
        )

        # Send colony and new food counter to the master process
        comm.send(obj=(colony, food_counter), dest=0, tag=DONE_TAG)


if rank == 0:
    master()
else:
    slave()
