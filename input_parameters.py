import sys
import re


def get_parameters(argv):
    size_laby = 25, 25
    if len(sys.argv) > 2:
        size_laby = int(sys.argv[1]), int(sys.argv[2])

    resolution = size_laby[1] * 8, size_laby[0] * 8
    nb_ants = size_laby[0] * size_laby[1] // 4

    max_life = 500
    if len(sys.argv) > 3:
        max_life = int(sys.argv[3])

    pos_food = size_laby[0] - 1, size_laby[1] - 1
    pos_nest = 0, 0

    alpha = 0.9
    beta = 0.99
    if len(sys.argv) > 4:
        alpha = float(sys.argv[4])
    if len(sys.argv) > 5:
        beta = float(sys.argv[5])

    max_iterations = None
    for match in filter(
        lambda a: isinstance(a, str) and a.startswith("max_iterations="), sys.argv
    ):
        max_iterations = int(re.search(r"\d+", match).group())

    n_tasks = None
    for match in filter(
        lambda a: isinstance(a, str) and a.startswith("n_tasks="), sys.argv
    ):
        n_tasks = int(re.search(r"\d+", match).group())

    return {
        "size_laby": size_laby,
        "resolution": resolution,
        "nb_ants": nb_ants,
        "max_life": max_life,
        "pos_food": pos_food,
        "pos_nest": pos_nest,
        "alpha": alpha,
        "beta": beta,
        "max_iterations": max_iterations,
        "n_tasks": n_tasks,
    }
