<center> <h1>OS202 Projet : Ants colonization optimisation</h1> </center>

**<p style="text-align: center;">Kateryna TARELKINA</p>**
**<p style="text-align: center;">2024</p>**

<!-- vscode-markdown-toc -->

- 1. [lscpu](#lscpu)
- 2. [Exécuter](#Excuter)
  - 2.1. [Version Parallélisée](#VersionParalllise)
- 3. [Parallélisation du code](#Paralllisationducode)
- 4. [Résultats](#Rsultats)
  - 4.1. [La taille de labyrinthe: 32](#Latailledelabyrinthe:32)
  - 4.2. [La taille de labyrinthe: 64](#Latailledelabyrinthe:64)
  - 4.3. [La taille de labyrinthe: 128](#Latailledelabyrinthe:128)

<!-- vscode-markdown-toc-config
	numbering=true
	autoSave=true
	/vscode-markdown-toc-config -->
<!-- /vscode-markdown-toc -->

## 1. <a name='lscpu'></a>lscpu

Les caractéristiques hardware sont présentées ci-dessous.

```
Architecture :                              x86_64
  Mode(s) opératoire(s) des processeurs :   32-bit, 64-bit
  Address sizes:                            39 bits physical, 48 bits virtual
  Boutisme :                                Little Endian
Processeur(s) :                             4
  Liste de processeur(s) en ligne :         0-3
Identifiant constructeur :                  GenuineIntel
  Nom de modèle :                           Intel(R) Core(TM) i3-8145U CPU @ 2.10GHz
    Famille de processeur :                 6
    Modèle :                                142
    Thread(s) par cœur :                    2
    Cœur(s) par socket :                    2
    Socket(s) :                             1
    Vitesse maximale du processeur en MHz : 3900,0000
    Vitesse minimale du processeur en MHz : 400,0000
Caches (sum of all):
  L1d:                                      64 KiB (2 instances)
  L1i:                                      64 KiB (2 instances)
  L2:                                       512 KiB (2 instances)
  L3:                                       4 MiB (1 instance)
```

## 2. <a name='Excuter'></a>Exécuter

```
python3 ants.py [height] [width] [max_life] [alpha] [beta] [max_iterations=n]
```

- height : hauteur du labyrinthe, 25 par défaut
- width : largeur du labyrinthe, 25 par défaut
- max_life : âge maximum de chaque fourmi, 500 par défaut
- alpha : paramètre de la phéromone, 0,9 par défaut. Le paramètre 𝛼 donne le taux de copie de phéromone, 1−𝛼 donne le taux de diffusion.
- beta : paramètre de la phéromone, 0,99 par défaut. Plus le paramètre 𝛽 s'approche de 0, plus les phéromones laissées par les fourmis vont disparaître rapidement, plus 𝛽 se rapprochera de 1, plus les phéromones seront persistantes.
- max_iterations : le programme s'arrêtera après un nombre spécifié d'itérations, donnant le temps FPS moyen et le compteur de nourriture final

### 2.1. <a name='VersionParalllise'></a>Version Parallélisée

```
mpirun -n [number_of_processes] python3 ./mpi.py [height] [width] [max_life] [alpha] [beta] [max_iterations=n] [n_tasks=m]
```

- n_tasks : correspond au nombre de colonies dans lesquelles les fourmis seront réparties. Chaque colonie sera traitée séparément par un des processus. Si aucune valeur n'est fournie, le nombre de processus est pris par défaut.

`--hostfile` peut être utilisé :

```
mpirun --hostfile hostfile -n [number_of_processes] python3 ./mpi.py [height] [width] [max_life] [alpha] [beta] [max_iterations=n] [n_tasks=m]
```

## 3. <a name='Paralllisationducode'></a>Parallélisation du code

D’abord, la stratégie maître-esclave a été utilisée avec le nombre des tâches supérieur au nombre des processus-esclaves. Mais vu que l’on sait que chaque tâche a la même complexité de calcul (comme les colonies ont toutes la même taille), cette stratégie n’est pas nécessaire. De plus, la granularité plus fine a donné de plus mauvaise performance que la version non-parallélisée.

Il a été décidé de choisir le nombre des tâches égale à n-1, où n – le nombre de processus. Ainsi, chaque processus-esclave n’obtient qu’une tâche et on améliore les performances du programme considérablement en diminuant le temps de communication.

Le processus responsable pour l’affichage est celui avec le rank 0 (root). Mais pour mesurer les performances on a commenté le code de l’interface graphique, car on a estimé que le temps d’affichage n’a pas d’impact assez considerable sur le temps d'exécution.

## 4. <a name='Rsultats'></a>Résultats

Les résultats obtenus sont donnés ci-dessous.

Les valuers par défaut:

- La taille de labyrinthe: 64

- Alpha: 0.9

- Beta: 0.99

### 4.1. <a name='Latailledelabyrinthe:32'></a>La taille de labyrinthe: 32

| Nombre de processeurs |   FPS | Speed-up (%) |
| --------------------: | ----: | -----------: |
|                     1 | 49.74 |          100 |
|                     2 | 52.67 |      94.4371 |
|                     3 | 47.85 |       103.95 |
|                     4 | 48.93 |      101.655 |
|                     5 | 22.41 |      221.954 |
|                     6 | 24.87 |          200 |
|                     7 | 11.91 |      417.632 |
|                     8 | 16.82 |      295.719 |

![FPS en fonction de nombre de processeurs. La taille de labyrinthe: 32.](plots/FPS_en_fonction_de_nombre_de_processeurs._La_taille_de_labyrinthe:_32..png)

### 4.2. <a name='Latailledelabyrinthe:64'></a>La taille de labyrinthe: 64

| Nombre de processeurs |   FPS | Speed-up (%) |
| --------------------: | ----: | -----------: |
|                     1 | 19.81 |          100 |
|                     2 | 18.21 |      108.786 |
|                     3 | 21.53 |      92.0111 |
|                     4 | 22.64 |         87.5 |
|                     5 | 14.54 |      136.245 |
|                     6 |  10.7 |       185.14 |
|                     7 | 13.79 |      143.655 |
|                     8 | 10.13 |      195.558 |

![FPS en fonction de nombre de processeurs. La taille de labyrinthe: 64.](plots/FPS_en_fonction_de_nombre_de_processeurs._La_taille_de_labyrinthe:_64..png)

### 4.3. <a name='Latailledelabyrinthe:128'></a>La taille de labyrinthe: 128

| Nombre de processeurs |  FPS | Speed-up (%) |
| --------------------: | ---: | -----------: |
|                     1 | 5.55 |          100 |
|                     2 | 4.64 |      119.612 |
|                     3 | 6.18 |      89.8058 |
|                     4 | 6.79 |      81.7378 |
|                     5 | 4.69 |      118.337 |
|                     6 | 5.06 |      109.684 |
|                     7 | 5.97 |      92.9648 |
|                     8 | 4.67 |      118.844 |

![FPS en fonction de nombre de processeurs. La taille de labyrinthe: 128.](plots/FPS_en_fonction_de_nombre_de_processeurs._La_taille_de_labyrinthe:_128..png)

---

On varie la taille du labyrinthe. On peut remarquer que la parallélisation est plus profitante pour les tailles plus petites. Cela peut être lié au fait que dans le cas d'un grand labyrinthe on perd du temps en échangeant les données entre le processus principal et les sous-processus.
